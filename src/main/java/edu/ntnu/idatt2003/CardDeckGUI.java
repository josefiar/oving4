package edu.ntnu.idatt2003;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import java.util.ArrayList;

/**
 * The CardDeckGUI class provides a graphical user interface for interacting with a deck of playing cards.
 * It allows users to deal hands of cards, check the properties of the hand, and manipulate the deck.
 */
public class CardDeckGUI extends Application {

  /** The deck of cards used in the GUI. */
  private DeckOfCards deckOfCards = new DeckOfCards();

  /** The hand of cards dealt in the GUI. */
  Hand handOfcards = new Hand(DeckOfCards.dealHand(5));

  /** Button to deal a hand of cards. */
  Button dealHandButton = new Button("Deal Hand");

  /** Button to check the properties of the hand. */
  Button checkHandButton = new Button("Check Hand");

  /** Button to fill up the deck with a standard set of cards. */
  Button fillUpDeckButton = new Button("Fill up deck");

  /** Text displaying the sum of face values of the cards in the hand. */
  Text sumofCards = new Text();

  /** Text displaying whether the hand has a flush. */
  Text flush = new Text();

  /** Text displaying the hearts cards in the hand. */
  Text hearts = new Text();

  /** Text displaying whether the hand contains the Queen of Spades. */
  Text queenOfSpades = new Text();

  /** Container for displaying cards in the hand. */
  HBox cardBox = new HBox();

  /**
   * Starts the JavaFX application by initializing the GUI components.
   * @param primaryStage The primary stage for the application.
   */
  @Override
  public void start(Stage primaryStage) {

    Label label = new Label();

    VBox checkHand = new VBox();

    dealHandButton.setOnAction(e -> {
      cardBox.getChildren().clear();
      handOfcards.playersHand(5);
      ArrayList<PlayingCard> hand = handOfcards.getHandCards();
      HBox cards = new HBox();
      for (PlayingCard card : hand) {
        cards.getChildren().add(new Label(card.getAsString()));
      }
      label.setText("Hand dealt: " );
      cardBox.getChildren().addAll(label, cards);
    });

    checkHandButton.setOnAction(e -> checkHand.getChildren().add(getResult()));

    fillUpDeckButton.setOnAction(e -> deckOfCards.fillUpTheDeck());

    HBox buttons = new HBox(50);
    buttons.setSpacing(20);
    buttons.getChildren().addAll(dealHandButton, checkHandButton, fillUpDeckButton);

    VBox root = new VBox(10);
    root.setPadding((new Insets(10)));
    root.getChildren().addAll(buttons, cardBox, checkHand);

    Scene scene = new Scene(root, 1000,1200);

    primaryStage.setTitle("CardGame");
    primaryStage.setScene(scene);
    primaryStage.show();
  }

  /**
   * Constructs a VBox containing the results of checking the properties of the hand.
   * @return A VBox containing the results.
   */
  private VBox getResult() {
    VBox result = new VBox();

    sumofCards.setText("Sum of Cards: " + handOfcards.getSumOfHand());

    flush.setText("Flush: " + handOfcards.hasFlush());

    if (handOfcards.checkHandForHearts().isEmpty()) {
      hearts.setText("No hearts");
    } else {
      hearts.setText("Check Hearts: " + handOfcards.checkHandForHearts());
    }

    queenOfSpades.setText("Check Queen of Spades: " + handOfcards.checkHandForQueenOfSpades());

    result.getChildren().addAll(sumofCards,flush,hearts,queenOfSpades);

    result.setAlignment(Pos.CENTER);

    return result;
  }

  /**
   * Launches the JavaFX application.
   * @param args The command-line arguments.
   */
  public static void main(String[] args) {
    launch(args);
  }
}