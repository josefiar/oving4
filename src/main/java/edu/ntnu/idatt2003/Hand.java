package edu.ntnu.idatt2003;

import java.util.ArrayList;
import java.util.stream.Collectors;

/**
 * The Hand class represents a player's hand in a card game.
 * It contains methods to manage and analyze the hand of playing cards.
 */
public class Hand {

  /** The list of playing cards in the hand. */
  private ArrayList<PlayingCard> hand;

  /**
   * Constructs a Hand object with the given list of playing cards.
   * @param dealHand The list of playing cards to initialize the hand with.
   */
  public Hand(ArrayList<PlayingCard> dealHand) {
    this.hand = dealHand;
  }

  /**
   * Deals a hand of cards of the specified size.
   * @param sizeHandCards The number of cards to deal for the hand.
   */
  public void playersHand(int sizeHandCards) {
    hand = DeckOfCards.dealHand(sizeHandCards);
  }

  /**
   * Retrieves the list of playing cards in the hand.
   * @return The list of playing cards in the hand.
   */
  public ArrayList<PlayingCard> getHandCards() {
    return hand;
  }

  /**
   * Calculates the sum of face values of the cards in the hand.
   * @return The sum of face values of the cards in the hand.
   */
  public int getSumOfHand() {
    return hand.stream().mapToInt(PlayingCard::getFace).sum();
  }

  /**
   * Checks if the hand has a flush: all cards have the same suit.
   * @return True if the hand has a flush, otherwise false.
   */
  public boolean hasFlush() {
    return hand.stream()
        .allMatch(card -> card.getSuit() == hand.get(0).getSuit());
  }

  /**
   * Checks the hand for cards of hearts suit and returns their string representations.
   * @return A list of string representations of hearts cards in the hand.
   */
  public ArrayList<String> checkHandForHearts() {
    return hand.stream()
        .filter(card -> card.getSuit() == 'H')
        .map(PlayingCard::getAsString)
        .collect(Collectors.toCollection(ArrayList::new));
  }

  /**
   * Checks if the hand contains the Queen of Spades.
   * @return True if the hand contains the Queen of Spades, otherwise false.
   */
  public boolean checkHandForQueenOfSpades() {
    return hand.stream()
        .anyMatch(card -> card.getAsString().equals("S12"));
  }

  /**
   * Returns a string representation of the hand.
   * @return A string representation of the hand.
   */
  @Override
  public String toString() {
    return "Hand{ hand = " + hand + '}';
  }
}