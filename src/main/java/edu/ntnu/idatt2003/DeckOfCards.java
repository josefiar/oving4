package edu.ntnu.idatt2003;

import java.util.ArrayList;
import java.util.Random;


/**
 * The DeckOfCards class represents a standard deck of playing cards.
 * It provides methods to initialize the deck, retrieve the deck, and deal hands of cards.
 */
public class DeckOfCards {

  /** The list of playing cards in the deck. */
  private static ArrayList<PlayingCard> deck;

  /**
   * Constructs a DeckOfCards object and initializes the deck with a standard set of playing cards.
   */
  public DeckOfCards() {
    deck = new ArrayList<>();
    fillUpTheDeck();
  }

  /**
   * Fills up the deck with a standard set of playing cards.
   * If the deck is not empty, it clears the existing cards before refilling.
   */
  public void fillUpTheDeck() {
    if (!deck.isEmpty()) {
      deck.clear();
    }
    for (int i = 1; i < 14; i++){
      deck.add(new PlayingCard('S', i));
      deck.add(new PlayingCard('H', i));
      deck.add(new PlayingCard('D', i));
      deck.add(new PlayingCard('C', i));
    }
  }

  public ArrayList<PlayingCard> getDeck() {
    return deck;
  }


  /**
   * Deals a hand of cards from the deck.
   * @param n The number of cards to deal for the hand.
   * @return An ArrayList containing the cards dealt for the hand.
   * @throws IllegalArgumentException if the specified number of cards is invalid (less than 1 or greater than 52).
   */
  public static ArrayList<PlayingCard> dealHand(int n) throws IllegalArgumentException {
    if (n < 1 || n > 52) {
      throw new IllegalArgumentException("Invalid number of cards requested");
    }

    ArrayList<PlayingCard> hand = new ArrayList<>();
    Random random = new Random();

    for (int i = 0; i < n; i++) {
      int randomIndex = random.nextInt(deck.size());
      hand.add(deck.get(randomIndex));
      deck.remove(randomIndex);
    }
    return hand;
  }
}