package edu.ntnu.idatt2003;

public class Main {

  /**
   * The main method to start the GUI.
   *
   * @param args the args
   */
  public static void main(String[] args) {

    CardDeckGUI.main(args);
  }
}