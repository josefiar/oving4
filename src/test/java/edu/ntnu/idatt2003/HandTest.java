package edu.ntnu.idatt2003;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class HandTest {

  ArrayList<PlayingCard> cards;
  Hand hand;
  @BeforeEach
  void setUp() {
    cards = new ArrayList<>();
    cards.add(new PlayingCard('S',1));
    cards.add(new PlayingCard('H',5));
    cards.add(new PlayingCard('D',10));
    hand = new Hand(cards);
  }

  @Test
  void getHandCards() {
    assertEquals(cards, hand.getHandCards());
  }

  @Test
  void getSumOfHand() {
    assertEquals(1+5+10, hand.getSumOfHand());
  }

  @Test
  void hasFlush() {
    assertFalse(hand.hasFlush());
  }

  @Test
  void checkHandForHearts() {
    ArrayList<String> hearts = hand.checkHandForHearts();
    assertEquals(1, hearts.size());
    assertTrue(hearts.contains("H5"));
  }

  @Test
  void checkHandForQueenOfSpades() {
    assertFalse(hand.checkHandForQueenOfSpades());
  }
}