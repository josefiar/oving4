package edu.ntnu.idatt2003;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class DeckOfCardsTest {
  private DeckOfCards deckOfCards;

  @BeforeEach
  void setUp() {
    deckOfCards = new DeckOfCards();
  }

  @Test
  void fillUpTheDeck() {
    deckOfCards.fillUpTheDeck();
    assertEquals(52, deckOfCards.getDeck().size());
  }

  @Test
  void dealHand() {
    ArrayList<PlayingCard> hand = deckOfCards.dealHand(5);
    assertEquals(5, hand.size());
    assertEquals(47, deckOfCards.getDeck().size());
  }

  @Test
  void dealHandInvalidNumberOfCards() {
    IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
      deckOfCards.dealHand(0);
    });
    assertEquals("Invalid number of cards requested", exception.getMessage());
  }
}